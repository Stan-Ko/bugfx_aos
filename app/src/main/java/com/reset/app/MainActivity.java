package com.reset.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    public static final String THEME_ID = "themeID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final int theme = getSharedPreferences(THEME_ID, Context.MODE_PRIVATE).getInt("theme", -1);
        if (theme != -1) {
            setTheme(R.style.DefaultMainTheme);
        }
    }
}
